# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Omar Polo <omar.polo@europecom.net>
# Copyright (C) 2017 Marcin Kurczewski <rr-@sakuya.pl>
# Copyright (C) 2017 Ricardo Ferreira <ricardo.sff@goatse.cx>
# Copyright (C) 2014 Charles Franklin <jakhead@gmail.com>
# Copyright (C) 2012 Markus Näsman <markus@botten.org>
# Copyright (C) 2011 David Flatz <david@upcs.at>
# Copyright (C) 2009 Bjorn Edstrom <be@bjrn.se>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


SCRIPT_NAME = "fish"
SCRIPT_AUTHOR = "Omar Polo <omar.polo@europecom.net>"
SCRIPT_VERSION = "0.1"
SCRIPT_LICENSE = "GPL3"
SCRIPT_DESC = "FiSH for weechat (with both py2 & py3 support)"
CONFIG_FILE_NAME = SCRIPT_NAME

import re
import struct
import hashlib
from os import urandom
import weechat
import base64
import Crypto.Cipher.Blowfish

# custom global var

fish_config_file = None
fish_config_section = {}
fish_config_option = {}
fish_keys = {}
fish_cyphers = {}
fish_encryption_announced = {}

class MalformedError(Exception):
    def __init__(self):
        self.boh = 'yup'
    def __str__(self):
        return "MalformedError"

# blowfish

class Blowfish:
    def __init__(self, key=None):
        if key:
            if len(key) > 72:
                key = key[:72]
            self.blowfish = Crypto.Cipher.Blowfish.new(key)

    def decrypt(self, data):
        return self.blowfish.decrypt(data)

    def encrypt(self, data):
        return self.blowfish.encrypt(data)

def blowcrypt_b64encode(s):
    """Encode binary data with base64"""
    return base64.b64encode(s)

def blowcrypt_b64decode(s):
    """Decode data from base64"""
    return base64.b64decode(s)

def padto(msg, length):
    """Pads 'msg' with zeroes until it's length is divisible by 'length'.
    If the length of msg is already a multiple of 'length', does nothing."""
    l = len(msg)
    missing = length - l % length
    return msg.ljust(l + missing, '\x00')

def blowcrypt_pack(msg, cipher):
    """."""
    encr = base64.b64encode(cipher.encrypt(padto(msg, 8)))
    try:
        return '+OK ' + encr
    except:
        return '+OK ' + encr.decode('ascii')

def blowcrypt_unpack(msg, cipher):
    """."""
    if not (msg.startswith('+OK ') or msg.startswith('mcps ')):
        raise ValueError
    _, rest = msg.split(' ', 1)
    if len(rest) < 12:
        raise MalformedError
    # rest = rest[:-(len(rest) % 12)]
    return cipher.decrypt(base64.b64decode(rest)).decode('utf-8', 'backslashreplace').strip('\x00')

    # if not (len(rest) %12) == 0:
    #     rest = rest[:-(len(rest) % 12)]

    # try:
    #     raw = blowcrypt_b64decode(padto(rest, 12))
    # except TypeError:
    #     raise MalformedError
    # if not raw:
    #     raise MalformedError

    # try:
    #     plain = cipher.decrypt(raw)
    # except ValueError as e:
    #     # weechat.prnt("", "ERROR: raw is {}, -- msg is ".format(raw, msg))
    #     weechat.prnt("", "error: %s" % e)
    #     raise MalformedError

    # return plain.decode('utf-8', 'backslashreplace').strip('\x00').replace('\n','')

# helper

def fish_announce_encrypted(buffer, target):
    global fish_encryption_announced, fish_config_option

    if (not weechat.config_boolean(fish_config_option['announce']) or
            fish_encryption_announced.get(target)):
        return

    (server, nick) = target.split("/")

    if (weechat.info_get("irc_is_nick", nick) and
            weechat.buffer_get_string(buffer, "localvar_type") != "private"):
        # if we get a private message and there no buffer yet, create one and
        # jump back to the previous buffer
        weechat.command(buffer, "/mute -all query %s" % nick)
        buffer = weechat.info_get("irc_buffer", "%s,%s" % (server, nick))
        weechat.command(buffer, "/input jump_previously_visited_buffer")

    fish_alert(buffer, "Messages to/from %s are encrypted." % target)
    fish_encryption_announced[target] = True

def fish_announce_unencrypted(buffer, target):
    global fish_encryption_announced, fish_config_option

    if (not weechat.config_boolean(fish_config_option['announce']) or
            not fish_encryption_announced.get(target)):
        return

    fish_alert(buffer, "Messages to/from %s are %s*not*%s encrypted." % (
            target,
            weechat.color(weechat.config_color(fish_config_option["alert"])),
            weechat.color("chat")))
    del fish_encryption_announced[target]


def fish_alert(buffer, message):
    mark = "%s%s%s\t" % (
            weechat.color(weechat.config_color(fish_config_option["alert"])),
            weechat.config_string(fish_config_option["marker"]),
            weechat.color("chat"))
    weechat.prnt(buffer, "%s%s" % (mark, message))

def fish_msg_w_marker(msg):
    marker = weechat.config_string(fish_config_option["mark_encrypted"])
    if weechat.config_string(fish_config_option["mark_position"]) == "end":
        return "%s%s" % (msg, marker)
    elif weechat.config_string(fish_config_option["mark_position"]) == "begin":
        return "%s%s" % (marker, msg)
    else:
        return msg

# hooks

def fish_modifier_in_notice_cb(data, modifier, server_name, string):
    global fish_DH1080ctx, fish_keys, fish_cyphers

    # we try to be fish.py compatible, so we use the same regex. DH1080 isn't implemented
    match = re.match(
        r"^(:(.*?)!.*? NOTICE (.*?) :)((DH1080_INIT |DH1080_FINISH |\+OK |mcps )?.*)$",
        string)
    #match.group(0): message
    #match.group(1): msg without payload
    #match.group(2): source
    #match.group(3): target
    #match.group(4): msg
    #match.group(5): DH1080_INIT |DH1080_FINISH 
    if not match or not match.group(5):
        return string

    # do not decrypt our message
    if match.group(3) != weechat.info_get("irc_nick", server_name):
        return string

    target = "{}/{}".format(server_name, match.group(2))
    target_lower = target.lower()
    buffer = weechat.info_get('irc_buffer', '{},{}'.format(server_name, match.group(2)))

    if match.group(5) in ["+OK "] and target_lower in fish_keys:
        if target_lower not in fish_cyphers:
            b = Blowfish(fish_keys[target_lower])
            fish_cyphers[target_lower] = b
        else:
            b = fish_cyphers[target_lower]
        clean = blowcrypt_unpack(match.group(4), b)
        fish_announce_encrypted(buffer, target)
        return "%s%s" % (match.group(1), fish_msg_w_marker(clean))

    fish_announce_unencrypted(buffer, target)

    return string


def fish_modifier_in_privmsg_cb(data, modifier, server_name, string):
    global fish_keys, fish_cyphers

    # try our best to be compatible with the original fish.py
    match = re.match(
        r"^(:(.*?)!.*? PRIVMSG (.*?) :)(\x01ACTION )?((\+OK |mcps )?.*?)(\x01)?$",
        string)
    #match.group(0): message
    #match.group(1): msg without payload
    #match.group(2): source
    #match.group(3): target
    #match.group(4): action
    #match.group(5): msg
    #match.group(6): +OK |mcps 
    if not match:
        return string

    if match.group(3) == weechat.info_get("irc_nick", server_name):
        dest = match.group(2)
    else:
        dest = match.group(3)
    target = "%s/%s" % (server_name, dest)
    targetl = ("%s/%s" % (server_name, dest)).lower()
    buffer = weechat.info_get("irc_buffer", "%s,%s" % (server_name, dest))

    if not match.group(6):
        fish_announce_unencrypted(buffer, target)
        return string

    if targetl not in fish_keys:
        fish_announce_unencrypted(buffer, target)
        return string

    fish_announce_encrypted(buffer, target)

    if targetl not in fish_cyphers:
        b = Blowfish(fish_keys[targetl])
        fish_cyphers[targetl] = b
    else:
        b = fish_cyphers[targetl]
    clean = blowcrypt_unpack(match.group(5), b)

    if not match.group(4):
        return "%s%s" % (match.group(1), fish_msg_w_marker(clean))

    return "%s%s%s\x01" % (match.group(1), match.group(4), fish_msg_w_marker(clean))

def fish_modifier_in_topic_cb(data, modifier, server_name, string):
    global fish_keys, fish_cyphers

    match = re.match(r"^(:.*?!.*? TOPIC (.*?) :)((\+OK |mcps )?.*)$", string)
    #match.group(0): message
    #match.group(1): msg without payload
    #match.group(2): channel
    #match.group(3): topic
    #match.group(4): +OK |mcps 
    if not match:
        return string

    target = "%s/%s" % (server_name, match.group(2))
    targetl = ("%s/%s" % (server_name, match.group(2))).lower()
    buffer = weechat.info_get("irc_buffer", "%s,%s" % (server_name,
            match.group(2)))

    if targetl not in fish_keys or not match.group(4):
        fish_announce_unencrypted(buffer, target)

        return string

    if targetl not in fish_cyphers:
        b = Blowfish(fish_keys[targetl])
        fish_cyphers[targetl] = b
    else:
        b = fish_cyphers[targetl]
    clean = blowcrypt_unpack(match.group(3), b)

    fish_announce_encrypted(buffer, target)

    return "%s%s" % (match.group(1), fish_msg_w_marker(clean))

def fish_modifier_in_332_cb(data, modifier, server_name, string):
    global fish_keys, fish_cyphers

    match = re.match(r"^(:.*? 332 .*? (.*?) :)((\+OK |mcps )?.*)$", string)
    if not match:
        return string

    target = "%s/%s" % (server_name, match.group(2))
    targetl = ("%s/%s" % (server_name, match.group(2))).lower()
    buffer = weechat.info_get("irc_buffer", "%s,%s" % (server_name,
            match.group(2)))

    if targetl not in fish_keys or not match.group(4):
        fish_announce_unencrypted(buffer, target)
        return string

    if targetl not in fish_cyphers:
        b = Blowfish(fish_keys[targetl])
        fish_cyphers[targetl] = b
    else:
        b = fish_cyphers[targetl]

    clean = blowcrypt_unpack(match.group(3), b)
    fish_announce_encrypted(buffer, target)
    return "%s%s" % (match.group(1), fish_msg_w_marker(clean))

def fish_modifier_out_privmsg_cb(data, modifier, server_name, string):
    global fish_keys, fish_cyphers

    match = re.match(r"^(PRIVMSG (.*?) :)(.*)$", string)
    if not match:
        return string

    target = "%s/%s" % (server_name, match.group(2))
    targetl = ("%s/%s" % (server_name, match.group(2))).lower()
    buffer = weechat.info_get("irc_buffer", "%s,%s" % (server_name,
            match.group(2)))

    if targetl not in fish_keys:
        fish_announce_unencrypted(buffer, target)
        return string

    if targetl not in fish_cyphers:
        b = Blowfish(fish_keys[targetl])
        fish_cyphers[targetl] = b
    else:
        b = fish_cyphers[targetl]

    cypher = blowcrypt_pack(match.group(3), b)
    fish_announce_encrypted(buffer, target)
    return "%s%s" % (match.group(1), cypher)

def fish_modifier_out_topic_cb(data, modifier, server_name, string):
    global fish_keys, fish_cyphers

    match = re.match(r"^(TOPIC (.*?) :)(.*)$", string)
    if not match:
        return string
    if not match.group(3):
        return string

    target = "%s/%s" % (server_name, match.group(2))
    targetl = ("%s/%s" % (server_name, match.group(2))).lower()
    buffer = weechat.info_get("irc_buffer", "%s,%s" % (server_name,
            match.group(2)))

    if targetl not in fish_keys:
        fish_announce_unencrypted(buffer, target)
        return string

    if targetl not in fish_cyphers:
        b = Blowfish(fish_keys[targetl])
        fish_cyphers[targetl] = b
    else:
        b = fish_cyphers[targetl]

    cypher = blowcrypt_pack(match.group(3), b)
    fish_announce_encrypted(buffer, target)
    return "%s%s" % (match.group(1), cypher)

# config related functions

def fish_config_reload_cb(data, config_file):
    return weechat.config_reload(config_file)

def fish_read_config():
    global fish_config_file
    return weechat.config_read(fish_config_file)

def fish_write_config():
    global fish_config_file
    return weechat.config_write(fish_config_file)

def fish_config_keys_read_cb(data, config_file, section_name, option_name, value):
    global fish_keys

    option = weechat.config_new_option(config_file, section_name, option_name,
            "string", "key", "", 0, 0, "", value, 0, "", "", "", "", "", "")
    if not option:
        return weechat.WEECHAT_CONFIG_OPTION_SET_ERROR

    fish_keys[option_name] = value

    return weechat.WEECHAT_CONFIG_OPTION_SET_OK_CHANGED

def fish_config_keys_write_cb(data, config_file, section_name):
    global fish_keys

    weechat.config_write_line(config_file, section_name, "")
    for target, key in sorted(fish_keys.items()):
        weechat.config_write_line(config_file, target, key)

    return weechat.WEECHAT_RC_OK

def fish_unload_cb():
    fish_write_config()
    return weechat.WEECHAT_RC_OK

def fish_init():
    global fish_config_file, fish_config_section, fish_config_option

    fish_config_file = weechat.config_new(CONFIG_FILE_NAME, "fish_config_reload_cb", "")
    if not fish_config_file:
        weechat.prnt("", "WARN: no config file!")
        return

    # look
    fish_config_section["look"] = weechat.config_new_section(fish_config_file,
            "look", 0, 0, "", "", "", "", "", "", "", "", "", "")

    if not fish_config_section["look"]:
        weechat.config_free(fish_config_file)
        return

    fish_config_option["announce"] = weechat.config_new_option(
            fish_config_file, fish_config_section["look"], "announce",
            "boolean", "annouce if messages are being encrypted or not", "", 0,
            0, "on", "on", 0, "", "", "", "", "", "")
    fish_config_option["marker"] = weechat.config_new_option(
            fish_config_file, fish_config_section["look"], "marker",
            "string", "marker for important FiSH messages", "", 0, 0, " :: ",
            " :: ", 0, "", "", "", "", "", "")

    fish_config_option["mark_position"] = weechat.config_new_option(
            fish_config_file, fish_config_section["look"], "mark_position",
            "integer", "where the marker for incoming encrypted message should be",
            "off|begin|end", 0, 2, "off", "off", 0, "", "", "", "", "", "")

    fish_config_option["mark_encrypted"] = weechat.config_new_option(
            fish_config_file, fish_config_section["look"], "mark_encrypted",
            "string", "marker for encrypted incoming messages", "", 0, 0, "*",
            "*" , 0, "", "", "", "", "", "")

    # color
    fish_config_section["color"] = weechat.config_new_section(fish_config_file,
            "color", 0, 0, "", "", "", "", "", "", "", "", "", "")
    if not fish_config_section["color"]:
        weechat.config_free(fish_config_file)
        return

    fish_config_option["alert"] = weechat.config_new_option(
            fish_config_file, fish_config_section["color"], "alert",
            "color", "color for important FiSH message markers", "", 0, 0,
            "red", "red", 0, "", "", "", "", "", "")

    # keys
    fish_config_section["keys"] = weechat.config_new_section(fish_config_file,
            "keys", 0, 0,
            "fish_config_keys_read_cb", "",
            "fish_config_keys_write_cb", "", "",
            "", "", "", "", "")
    if not fish_config_section["keys"]:
        weechat.config_free(fish_config_file)
        return

# commands

def fish_cmd_blowkey(data, buffer, args):
    argv = args.split(" ")

    if len(argv) == 0 or argv[0] == "":
        argv = ["help"]

    if argv[0] == "list":
        weechat.prnt("", "Loaded keys:")
        for target, _ in sorted(fish_keys.items()):
            weechat.prnt("", " - {}".format(target))
    elif argv[0] == "help":
        weechat.prnt("", "help command")
    elif argv[0] == "add" or argv[0] == "update": # at the end of the day, add
                                                  # and update are quite the
                                                  # same thing
        if len(argv) != 3:
            weechat.prnt("", "invalid parameter. Usage: /blowkey {} target key".format(argv[0]))
            return weechat.WEECHAT_RC_ERROR

        _, target, key = argv
        server_name = weechat.buffer_get_string(buffer, "localvar_server")
        fish_keys["{}/{}".format(server_name, target)] = key
        weechat.prnt("", "set key for {}".format(target))

    elif argv[0] == "del":
        if len(argv) != 2:
            weechat.prnt("", "invalid parameter. Usage: /blowkey {} target".format(argv[0]))
            return weechat.WEECHAT_RC_ERROR

        _, target = argv
        server_name = weechat.buffer_get_string(buffer, "localvar_server")
        del fish_keys["{}/{}".format(server_name, target)]
        weechat.prnt("", "deleted key for {}".format(target))

    else:
        weechat.prnt("", "unknown command")

    return weechat.WEECHAT_RC_OK

# init

if (__name__ == "__main__" and weechat.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION,
                                                SCRIPT_LICENSE, SCRIPT_DESC, "fish_unload_cb", "")):
    weechat.hook_command("blowkey", "Manage FiSH keys",
            '[list] | [add target key] | [del target] | [update target newkey]',
            'description',
            'list'
            ' || help'
            ' || add %(irc_channel)|%(nicks)'
            ' || del %(irc_channel)|%(nicks)'
            ' || update %(irc_channel)|%(nicks)',
            'fish_cmd_blowkey', '')

    fish_init()
    fish_read_config()

    weechat.hook_modifier("irc_in_notice", "fish_modifier_in_notice_cb", "")
    weechat.hook_modifier("irc_in_privmsg", "fish_modifier_in_privmsg_cb", "")
    weechat.hook_modifier("irc_in_topic", "fish_modifier_in_topic_cb", "")
    weechat.hook_modifier("irc_in_332", "fish_modifier_in_332_cb", "")
    weechat.hook_modifier("irc_out_privmsg", "fish_modifier_out_privmsg_cb", "")
    weechat.hook_modifier("irc_out_topic", "fish_modifier_out_topic_cb", "")


