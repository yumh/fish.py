# fish.py

> Teach to weechat how to swim like a fish, with python{2,3} support

This is a partial rewrite of the
[fish.py](https://weechat.org/scripts/source/fish.py.html/) weechat
plugin with python 2 *and* 3 support.

## Why a fork?

Two word: python3 support. Some platforms offer only weechat with
python3 support and I'd like to use those as well to write encrypted
messages: improving the plugin seemed more reasonable than rebuild
weechat from scratch with py2 support.

## Difference with the original version

 - **both python 2 and 3 support**
 
   the original version supports only python2. This support both
   version
   
 - DH removed
 
 - **standard base64**
 
   Honestly, I don't know why the author(s) chosen to write a custom
   base64 encoder/decoder. The python standard one seem to work well,
   so why don't use that? **The drawback is that message written with
   this plugin cannot be decrypted with the original plugin and vice
   versa!** but the good thing is that it'll be more easy to port this
   plugin to other languages/client.
   
## Usage

 - Install the plugin
 
 - If you used the original version all your keys will be
   imported. The options management is quite the same, so you won't
   have to worry about compatibility issue jumping back and forth this
   plugin and the original one.
   
 - RTFM
   ```
   /help blowkey
   ```
   
 - profit!
